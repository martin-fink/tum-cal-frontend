# TUM calendar proxy frontend

This project provides a frontend to [https://cal.bruck.me/](https://cal.bruck.me/).

## Docker

This project uses docker for development and deployment. If you are developing locally,
the `tools/` directory provides access to commands like `yarn`, so it feels as if you were
developing on your machine. The main advantage is that you don't have to install dependencies
like yarn on your machine and keep it clutter-free.

If you want, you can still use yarn, without using docker.

## Setup and development

```bash
$ tools/yarn install

# Run the vue dev server
$ docker-compose up
```

Adding a dependency

```bash
$ tools/yarn add dependency
```

Running a non-yarn command in the docker container

```bash
$ tools/exec your-command arg1 arg2 arg3
```

## Contributing

Feel free to fork the project!
