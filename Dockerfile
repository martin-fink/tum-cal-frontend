FROM node:9 as stage-build

MAINTAINER Martin Fink <martin@finkmartin.com>

WORKDIR /app

COPY package.json yarn.lock /app/

RUN yarn install --frozen-lockfile

COPY . /app

RUN yarn build

FROM nginx:mainline-alpine

COPY --from=stage-build /app/dist/ /usr/share/nginx/html
COPY --from=stage-build /app/nginx.conf /etc/nginx/conf.d/default.conf
