export default {
  index: {
    form: {
      errors: {
        pstud: 'Missing parameter pStud.',
        ptoken: 'Missing parameter pToken.',
        invalid_url: 'Invalid URL entered. Please check your input.',
      },
    },
  },
}
