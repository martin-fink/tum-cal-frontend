export default {
  index: {
    form: {
      errors: {
        pstud: 'Parameter pStud fehlt.',
        ptoken: 'Parameter pToken fehlt.',
        invalid_url: 'Die eingegebene URL ist nicht gültig. Bitte überprüfen Sie Ihre Eingabe.',
      },
    },
  },
}
